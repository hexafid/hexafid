# MIT License
# Copyright (c) 2020 h3ky1

# Standard library imports
import collections
import base64
import random
import math
import re
# Third party imports
# Local application imports
import hexafid.hexafid_core as hexafid

SYMBOLS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
BITS = '01'
HEXABITS = 6
ASCIIBITS = 8


def quad(A, B, C):
    """Cross product of elements in A, B, and C."""
    return [a + b + c for a in A for b in B for c in C]


quadresses = []
cells = []
peers = []
units = []
# units = dict((s, [u for u in unitlist if s in u])
# unitlist = ([cross(rows, c) for c in cols] + [cross(r,
# cols) for r in rows] + [cross(rs, cs) for rs in ('ABC','DEF','GHI') for cs in ('123','456','789')])

# blocklist = ([])
# blocks = dict((c, [b for b in blocklist if c in b]))


def get_precedence(plaintext, ciphertext):
    """most repeating characters across both ciphertext and plaintext, then most repeating within plaintext or
    ciphertext, then non-repeating, then all others """

    # possible improvement is to adjust precedence from least to most bit value possibilities

    constrained_list = []
    repeating_list = []

    char_ctr = collections.Counter(plaintext + ciphertext)
    for char, count in char_ctr.most_common():
        if char in SYMBOLS:
            if char in plaintext and char in ciphertext:
                constrained_list.append(char)
            else:
                repeating_list.append(char)

    constrained = ''.join(constrained_list) + ''.join(repeating_list)

    rem_string = SYMBOLS
    for c in constrained:
        rem_string = rem_string.replace(c, '')

        # randomize order or remaining characters
        rem_list = list(rem_string)
        random.SystemRandom().shuffle(rem_list)

    precedence = constrained + ''.join(rem_list)

    return precedence


# def padPair(plaintext, stegatext):

# return plaintext, stegatext


def parse_keysquare(keysquare):
    """Convert keysquare to a dict of possible values, {quadress: symbols},
    or return False if a contradiction is detected. """
    # To start, every cell can be any symbol; then assign values from the keysquare.
    values = dict((q, SYMBOLS) for q in quadresses)
    for q, s in keysquare_values(keysquare).items():
        if s in SYMBOLS and not assign(values, q, s):
            return False  # (Fail if we can't assign s to quadress q.)
    return values


def keysquare_values(keysquare):
    "Convert keysquare into a dict of {quadress: char} with '.' for empties."
    chars = [c for c in keysquare if c in SYMBOLS or c in '.']
    assert len(chars) == 64
    return dict(zip(keysquare, chars))


def parse_block(block):
    """Convert block to a dict of possible values, {cell: bits}, or return False if a contradiction is detected."""
    # To start, every cell can be any bit; then assign values from the block.
    values = dict((c, BITS) for c in cells)
    for c, b in block_values(block).items():
        if b in BITS and not assign(values, c, b):
            return False  # (Fail if we can't assign b to cell c.)
    return values


def block_values(block):
    "Convert block into a dict of {cell: bit} with '.' for empties."
    chars = [c for c in block if c in BITS or c in '.']
    assert len(chars) == 2
    return dict(zip(block, chars))


def assign(values, address, symbols):
    """Eliminate all the other values (except symbols) from values[address] and propagate. Return values,
    except return False if a contradiction is detected. """
    other_values = values[address].replace(symbols, '')
    if all(eliminate(values, address, symbols2) for symbols2 in other_values):
        return values
    else:
        return False


def eliminate(values, address, symbols):
    """Eliminate symbols from values[address]; propagate when values or places <= 2. Return values, except return
    False if a contradiction is detected. """
    if symbols not in values[address]:
        return values  # Already eliminated
    values[address] = values[address].replace(symbols, '')

    # (1) If a cell address is reduced to one value symbols2, then eliminate symbols2 from the peers.
    if len(values[address]) == 0:
        return False  # Contradiction: removed last value
    elif len(values[address]) == 1:
        symbols2 = values[address]
        if not all(eliminate(values, address2, symbols2) for address2 in peers[address]):
            return False

    # (2) If a block b (unit u) is reduced to only one place for a value symbols, then put it there.
    for u in units[address]:
        symbolplaces = [address for address in u if symbols in values[address]]
    if len(symbolplaces) == 0:
        return False  # Contradiction: no place for this value
    elif len(symbolplaces) == 1:
        # symbol can only be in one place in unit; assign it there
        if not assign(values, symbolplaces[0], symbols):
            return False

    return values


def get_quadress(keysquare):
    quadress, possible_symbols = random.SystemRandom().choice(list(keysquare.items()))

    while len(possible_symbols) == 1:
        quadress, possible_symbols = random.SystemRandom().choice(list(keysquare.items()))

    print(quadress)
    print(possible_symbols)

    return quadress


def build_keyquare(key):
    quads = ['00', '01', '10', '11']
    keysquare = {}

    for top in quads:
        for middle in quads:
            for bottom in quads:
                quadress = top + middle + bottom
                keysquare[quadress] = key

    return keysquare


def main():
    real_message = 'It works'
    fake_message = 'Hello'
    period = 5

    # prepare real_message
    plaintext = hexafid.prepare_message(real_message, separator=False)

    # prepare fake_message
    stegatext = re.sub(r'[^A-Za-z0-9+/]+', '', fake_message)

    # alternately pad pt or st as needed
    # num_plainbits = len(plaintext) * HEXABITS
    # num_stegachars = int(num_plainbits / ASCIIBITS)

    while (len(plaintext) * HEXABITS) <= (len(stegatext) * ASCIIBITS):
        if '++' in plaintext:
            plaintext += random.SystemRandom().choice(SYMBOLS)
        else:
            plaintext += '+'

    while len(stegatext) * ASCIIBITS <= (len(plaintext) * HEXABITS) - 3:
        if '++' in stegatext:
            stegatext += random.SystemRandom().choice(SYMBOLS)
        else:
            stegatext += '+'

    stegatext_b64 = base64.b64encode(stegatext.encode())
    ciphertext = stegatext_b64.decode('utf-8')

    print(plaintext)
    print(stegatext)
    print(ciphertext)

    # getCipherText()

    # create symbol precedence list for search
    precedence = get_precedence(plaintext, ciphertext)
    print(precedence)

    # work through key solutions like sudoku
    # Peter Norvig article on constraint propagation and search in sudoku

    # steganography

    # For each cell in the keysquare (i.e. quadress), consider all possible characters (i.e. A-Za-z0-9+/)
    keysquare = build_keyquare(SYMBOLS)

    # For each cell in each transposition block, consider all possible bit values (i.e. 0, 1)
    block = collections.namedtuple('block', ['num', 'col', 'row'])
    block_num = math.ceil(len(plaintext) / period)

    # for num in range(block_num):
    #     for col in range(min(period, (len(plaintext) - (num * period)))):
    #         for row in range(HEXABITS):
    #             cell = {block(num, col, row): BITS}
    #             print(cell)
    cell = ({block(num, col, row): BITS}
            for num in range(block_num)
            for col in range(min(period, (len(plaintext) - (num * period))))
            for row in range(HEXABITS))
    print(cell)

    # Moving from most constrained to least constrained character
    for char in precedence:
        # Select a quadress from the remaining possibilities in the keysquare
        # select least constrained or random
        quadress = get_quadress(keysquare)
        print(quadress)

        # For each transposition block,
        for num in range(block_num):

            # For each plaintext character substitution,
            # Evaluate the quadress as a plaintext character substitution (i.e. write vertically)
            for ptchar in plaintext:
                # get index
                ptchar


# cryptanalysis - for all letter in character set {all 64 bit combos} then run through block algorithm remove any
# conflicts after resolving all constraints, to have solution


if __name__ == '__main__':
    main()
