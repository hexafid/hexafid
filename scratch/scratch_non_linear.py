# MIT License
# Copyright (c) 2020 h3ky1

import hexafid.hexafid_keygen as keygen
import hexafid.hexafid_core as hexafid
import numpy as np

SYMBOLS = hexafid.SYMBOLS


# https://en.wikipedia.org/wiki/Fast_Walsh%E2%80%93Hadamard_transform
def fwht(a) -> None:
    """In-place Fast Walsh–Hadamard Transform of array a."""
    h = 1
    while h < len(a):
        for i in range(0, len(a), h * 2):
            for j in range(i, i + h):
                x = a[j]
                y = a[j + h]
                a[j] = x + y
                a[j + h] = x - y
        h *= 2


def sbox_encoders(encoder, decoder):
    """Return encoder dicts with keys substituted against almost perfect non-linear (APN) s-box;
    6 bit s-box from FIDES (https://eprint.iacr.org/2015/424.pdf)."""

    sbox = [54, 0, 48, 13, 15, 18, 35, 53, 63, 25, 45, 52, 3, 20, 33, 41,
            8, 10, 57, 37, 59, 36, 34, 2, 26, 50, 58, 24, 60, 19, 14, 42,
            46, 61, 5, 49, 31, 11, 28, 4, 12, 30, 55, 22, 9, 6, 32, 23,
            27, 39, 21, 17, 16, 29, 62, 1, 40, 47, 51, 56, 7, 43, 38, 44]

    # extract values from dicts
    encoder_list = list(encoder)
    decoder_list = list(decoder)

    # substitute key values for s-box values
    sboxed_encoder_list = [encoder_list[c] for c in [sbox[i] for i in range(64)]]
    sboxed_decoder_list = [decoder_list[q] for q in [sbox[i] for i in range(64)]]

    # replace key values into dictionaries
    sboxed_encoder = dict(zip(sboxed_encoder_list, sboxed_decoder_list))
    sboxed_decoder = dict(zip(sboxed_decoder_list, sboxed_encoder_list))

    return sboxed_encoder, sboxed_decoder


def row_shift():
    # for n in range(1, 65):
    print('(row * gen) % 8')
    for n in range(1, 9):  # once modded to % 8
        gen = n % 8
        print('KeyMod N%s ' % n, end='')
        for row in range(8):
            print((row * gen) % 8, end='')  # 8 unique shifts - 1 with all 0s (no shift)
        print()

    print('(gen ** row) % 8')
    for n in range(1, 9):  # once modded to % 8
        gen = n % 8
        print('KeyMod N%s ' % n, end='')
        for row in range(8):
            # print(((g * r) ** r) % 8, end='')  # 8 unique shifts
            print((gen ** row) % 8, end='')  # 8 unique shifts
        print()

    print('(row ** gen) % 8')
    for n in range(1, 9):  # once modded to % 8
        gen = n % 8
        print('KeyMod N%s ' % n, end='')
        for row in range(8):
            print((row ** gen) % 8, end='')  # 5 unique shifts, 2 repeats
        print()


def mix_columns(encoder, decoder):
    """
    return encoder dicts with columns mix through matrix transformation

    :param encoder:
    :param decoder:
    :return:
    """
    # key = ''.join(list(encoder))
    # row, shifted_row = [], []
    # col = 8  # for the 64 char key this equates to an 8x8 matrix
    # for r in range(int(len(key) / col)):
    #     row.append(key[r * col:(r + 1) * col])
    #
    # print()

    decoder_list = list(decoder)
    decoder_array = np.array(decoder_list).reshape(8, 8)
    mixing_array = decoder_array
    mixed = np.multiply(decoder_array, mixing_array)
    flat = np.ndarray.flatten(mixed)
    key = ''.join([str(x) for x in flat])

    new_encoder, new_decoder = hexafid.setup_quadtree_addresses(key)

    return new_encoder, new_decoder


def main():
    a = [x for x in range(64)]
    print(a)
    fwht(a)
    print(a)
    print()

    key = keygen.get_key_from_pass('Swordfish2007', 'forward')
    encoder, decoder = hexafid.setup_quadtree_addresses(key)
    print(encoder)
    boxed_encoder, boxed_decoder = sbox_encoders(encoder, decoder)
    print(boxed_encoder)
    boxed_seq_encoder, boxed_seq_decoder = hexafid.setup_sequence_addresses(key)
    mixed_encoder, mixed_decoder = mix_columns(boxed_seq_encoder, boxed_seq_decoder)
    print(mixed_encoder)

    row_shift()  # shift rows experiment for key schedule


if __name__ == '__main__':
    main()
