.. _cli-documentation:

Hexafid CLI
===========

As an end user, you can execute commands in your `virtual environment`_
like this:

.. code-block:: console

   $ hexafid --help

You will have access to the following options:

.. click:: hexafid.hexafid_cli:main
   :prog: hexafid
   :show-nested:

.. _virtual environment: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/
